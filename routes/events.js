const express = require('express')
const router = express.Router()
const Event = require('../models/Event')

const getEvents = async function (req, res, next) {
  try {
    console.log(req.query)
    const startData = req.query.starDate
    const endDate = req.query.endDate
    const events = await Event.find({
      $or: [{ starDate: { $gte: startData, $lt: endDate } },
        { endDate: { $gte: startData, $lt: endDate } }]
    }).exec()
    res.status(200).json(events)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
router.get('/', getEvents) // GET Products

module.exports = router
